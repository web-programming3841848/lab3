import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

interface AddressBook{
  id: number
  name: string
  tel: string
  gender: string
}
export const useAddressBookStore = defineStore('address_book', () => {
  
  let lastId = 1
  const isAddNew = ref(false)
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel:'',
    gender: 'Male'
  })
  
  const addressList = ref<AddressBook[]>([])
  function save(){
    if(address.value.id>0){
      //edit
      const editedIndex = addressList.value.findIndex((item)=>item.id===address.value.id)
      addressList.value[editedIndex] = address.value
  
    }else{
      //add new
      addressList.value.push({...address.value,id: lastId++})
    }
    address.value = {
    id: 0,
    name: '',
    tel:'',
    gender: 'Male'
    }
    isAddNew.value=false
  }
  function cancel(){
    address.value = {
    id: 0,
    name: '',
    tel:'',
    gender: 'Male'
    }
    isAddNew.value=false
  }
  function edit(id:number){
    isAddNew.value=true
    const editedIndex = addressList.value.findIndex((item)=>item.id===id)
    //copy Object JSON.parse(JSON.stringify(Object))
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  }
  function remove(id:number){
    const deletedIndex = addressList.value.findIndex((item)=>item.id===id)
    //copy Object JSON.parse(JSON.stringify(Object))
    addressList.value.splice(deletedIndex,1)
  }
  return { address,addressList,isAddNew,cancel,save,edit,remove}
})
